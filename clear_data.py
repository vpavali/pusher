#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rados
import pymongo

mongo = pymongo.MongoClient(
            host='127.0.0.1', 
            port=27017)

mongo.db.slices.drop()
mongo.db.segments.drop()

ceph = rados.Rados(conffile="/etc/ceph/ceph.conf", conf = dict (keyring = "/etc/ceph/ceph.client.admin.keyring"))

ceph.connect()

rados_ioctx = ceph.open_ioctx('segments')

[rados_ioctx.remove_object(str(o.key)) for o in rados_ioctx.list_objects()]

